//
//  Storyboardable.swift
//  Stay Home
//
//  Created by Igor Shelepov on 23/03/2020.
//

import UIKit

protocol Storyboardable: NSObjectProtocol {
    associatedtype Instance
    static func makeFromStoryboard() -> Instance
    static var storyboard: UIStoryboard { get }
    static var storyboardName: String { get }
    static var identifier: String { get }
}

extension Storyboardable {
    static var storyboardName: String {
        return className
    }
    
    static var identifier: String {
        return className
    }
    
    static var storyboard: UIStoryboard {
        return UIStoryboard(name: storyboardName, bundle: nil)
    }
    
    static func makeFromStoryboard() -> Self {
        return storyboard.instantiateViewController(withIdentifier: identifier) as! Self
    }
    
    static func makeFrom(storyboard: String) -> Self {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! Self
    }
    
    static func makeFrom(storyboard: String, vcIdentifier: String) -> Self {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: vcIdentifier) as! Self
    }
}

extension NSObjectProtocol {
    static var className: String {
        return String(describing: self)
    }
}
