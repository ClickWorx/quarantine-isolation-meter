//
//  AboutScreenViewController.swift
//  Stay Home
//
//  Created by Дмитрий Кривенко on 25.03.2020.
//

import Foundation
import UIKit

class AboutScreenViewController: UIViewController, Storyboardable {
    static var storyboardName: String {
        return "AboutScreen"
    }
    
    @IBOutlet var bottomButton: UIButton!
    
    @IBAction func onTapBeginQuarantineButton(_ sender: Any) {
        if fromSettings {
            navigationController?.popToRootViewController(animated: true)
        } else {
            defaultNavigate()
        }
    }
    
    @IBOutlet var bottomContentTextView: UITextView!
    
    public var fromSettings: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setSourceCodeLink()
        setBeginQuarantineButtonText()
    }
    
    private func setBeginQuarantineButtonText() {
        if fromSettings {
            bottomButton.setTitle("OK", for: .normal)
        }
    }
    
    private func setSourceCodeLink() {
        let attributedString = NSMutableAttributedString(string: "a publically available Bitbucket repository")
        let url = URL(string: "https://bitbucket.org/ClickWorx/quarantine-isolation-meter.git")!
        let systemFont = UIFont.systemFont(ofSize: 17, weight: .bold)
        
        attributedString.setAttributes([.link: url, .font: systemFont], range: NSMakeRange(0, attributedString.length))
        
        let baseText = NSMutableAttributedString(string: "PRIVACY is KEY! All personal location data is maintained exclusively on your phone.  This is not a effort to capture your personal data for monetization.  To prove this the application's source code is maintained in ", attributes: [.font: systemFont])
        baseText.append(attributedString)
        bottomContentTextView.isUserInteractionEnabled = true

        bottomContentTextView.linkTextAttributes = [
            .foregroundColor: UIColor.blue,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        bottomContentTextView.attributedText = baseText
    }
    
    private func defaultNavigate() {
        var vc: UIViewController
        let defaults = UserDefaults.standard
        
        defaults.set(true, forKey: "aboutScreenComplete")

        if (defaults.value(forKey: "lat") != nil && defaults.value(forKey: "lon") != nil && defaults.value(forKey: "radius") != nil) {
            vc = MainScreenTabBarController.makeFromStoryboard()
            self.navigationController?.setViewControllers([vc], animated: true)
        } else {
            let vc = LocationScreenViewController.makeFromStoryboard()
            vc.fromAbout = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        adjustUITextViewHeight(arg: bottomContentTextView)
        view.layoutIfNeeded()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
}
