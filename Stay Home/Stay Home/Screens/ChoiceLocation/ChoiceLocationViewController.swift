//
//  ChoiceLocationViewController.swift
//  Stay Home
//
//  Created by Дмитрий Кривенко on 26.03.2020.
//

import UIKit
import MapKit

class ChoiceLocationViewController: UIViewController, Storyboardable {
    static var storyboardName: String {
        return "ChoiceLocationScreen"
    }
    

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var searchResultsTable: UITableView!
    @IBOutlet var tableBottomConstraint: NSLayoutConstraint!
    
    var isSearch: Bool = false
    var searchController: UISearchController?
    let locationManager = CLLocationManager()
    var matchingItems: [MKMapItem] = []
    weak var mapView: MKMapView?
    var onSelectPlaceMark: ((MKPlacemark) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let a = presentationController.
        
        searchBar.delegate = self
        searchResultsTable.delegate = self
        searchResultsTable.dataSource = self
        
        searchController = UISearchController(searchResultsController: self)
        searchController?.searchResultsUpdater = self
        searchController?.obscuresBackgroundDuringPresentation = false
        searchController?.searchBar.placeholder = "Search Location"
        definesPresentationContext = true
        searchBar.searchTextField.textColor = .white
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)

        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = UIColor(red: 0.205, green: 0.205, blue: 0.205, alpha: 1.0)

        UINavigationBar.appearance().isTranslucent = false

        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.compactAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        
        setGrabber()
        setManifityGlassColor()
        
        searchBar.becomeFirstResponder()
    }
    
    func setManifityGlassColor() {
        let textField = searchBar.value(forKey: "searchField") as! UITextField

        let glassIconView = textField.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        glassIconView.tintColor = UIColor(red: 0.922, green: 0.922, blue: 0.961, alpha: 0.6)
    }
    
    func setGrabber() {
        let grabberLabel = UIView()

        grabberLabel.frame = CGRect(x: 0, y: 0, width: 36, height: 5)
        grabberLabel.backgroundColor = .clear
        grabberLabel.layer.backgroundColor = UIColor(red: 0.388, green: 0.388, blue: 0.4, alpha: 1).cgColor
        grabberLabel.layer.cornerRadius = 4
        navigationItem.titleView = grabberLabel
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            tableBottomConstraint.constant = 0.0
        } else {
            tableBottomConstraint.constant = keyboardViewEndFrame.height
        }
    }
    
    private func onSearch(searchText: String) {
        if searchText.count > 3 {
            let request = MKLocalSearch.Request()
            
            request.naturalLanguageQuery = searchText
            if let mv = mapView {
                request.region = mv.region
            }

            let search = MKLocalSearch(request: request)
            search.start { (response, error) in
                guard let response = response else {
                    return
                }
                
                self.matchingItems = response.mapItems
                self.searchResultsTable.reloadData()
            }
        } else {
            self.matchingItems = []
            self.searchResultsTable.reloadData()
        }
    }
    

    
    deinit {
        onSelectPlaceMark = nil
    }
}

extension ChoiceLocationViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearch = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder();
        isSearch = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchText = searchBar.text {
            isSearch = false
            onSearch(searchText: searchText)
        }
        searchBar.resignFirstResponder();
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        onSearch(searchText: searchText)
    }
}

extension ChoiceLocationViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    
  }
}

extension ChoiceLocationViewController: UITableViewDelegate {
}

extension ChoiceLocationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingItems.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = matchingItems[indexPath.row].placemark
        
        dismiss(animated: true, completion: { self.onSelectPlaceMark?(selectedItem) })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = matchingItems[indexPath.row].name
        cell.detailTextLabel?.text = matchingItems[indexPath.row].placemark.title
        cell.backgroundColor = UIColor.init(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.0)
        cell.selectionStyle = .none
        cell.textLabel?.textColor = .white
        cell.detailTextLabel?.textColor = .white
        
        return cell
    }
}

extension ChoiceLocationViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.first != nil {
            print("location:: (location)")
        }
    }

    private func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("error:: (error)")
    }
}
