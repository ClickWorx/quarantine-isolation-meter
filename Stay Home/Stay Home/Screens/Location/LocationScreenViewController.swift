//
//  LocationScreenViewController.swift
//  Stay Home
//
//  Created by Anton Trofimenko on 24.03.2020.
//

import UIKit
import MapKit

protocol HandleMapSearch {
    func dropPinZoomIn(placemark: MKPlacemark)
}

protocol LocationPanelDelegate {
    func navigateToHomeLocation()
    func navigateToHomeLocationAndUpdateView()
}

protocol UserDefaultHelper {
    func saveResultsToDefaults(lat: Double, long: Double, radius: Int, callback: @escaping ()-> Void)
}

class LocationScreenViewController: UIViewController, Storyboardable {
    static var storyboardName: String {
        return "LocationScreen"
    }
    
    public var fromAbout = false
    public var fromSettings = false
    
    public var setHomeBase = false
    public var setRadius = false
    
    private var selectedPickerIndex: Int = 5
    
    private var statusTimer: Timer?
    private var statusDeniedTimer: Timer?
    private var pin: MKPointAnnotation?
    private var circle: MKCircle?
    private var currentLocationName: String?
    
    
    private let pickerValues = [Int](50...500).filter { (i) -> Bool in
        return i % 10 == 0
    }
    
    private lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
//        manager.allowsBackgroundLocationUpdates = true
//        manager.pausesLocationUpdatesAutomatically = false
        return manager
    }()
    
    private var currentLocation: CLLocation? {
        didSet {
            updateCurrentLocationName()
        }
    }
    
    private var searchResult: MKPlacemark? {
        didSet {
            if (self.searchResult != nil) {
                updateSearchResultLocationName()
            } else {
                updateCurrentLocationName()
            }
        }
    }
    
    private var locationNameInitialized: Bool? {
        willSet {
            if (self.locationNameInitialized == nil || self.locationNameInitialized == false) && newValue != nil && newValue == true {
                self.updateLoadingView()
            }
        }
    }
    
    @IBOutlet weak var currentLocationPanelView: LocationCurrentPlacePanel!
    @IBOutlet weak var searchBarView: UISearchBar!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var bottomSearchConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var pickerViewContainer: UIView!
    @IBOutlet weak var searchViewContainer: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var cnstCancelBtnTop: NSLayoutConstraint!
    @IBOutlet weak var cnstSearchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cnstCancelBtnBottom: NSLayoutConstraint!
    @IBOutlet weak var btnCancelSelection: UIButton!
    @IBOutlet weak var loadingContainer: UIView!
    
    private var prevLocationStatus: CLAuthorizationStatus?
    private var ticksCount: Int = 0
    private var timeToWaitLocationOn: Int = 10
    private var isActive = true
    
    @IBAction func onCompletePickerClick(_ sender: Any) {
        if self.fromAbout || self.setRadius {
            self.saveLocation()
        } else {
            saveLocationWithAlert()
        }
    }
    
    @IBAction func onCancelPickerClick(_ sender: Any) {
        if setRadius {
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            updateViews(showPicker: false)
            searchResult = nil
            navigateToHomeLocation()
        }
    }
    
    @IBAction func onTapSearchBar(_ sender: Any) {
        showChoiceLocation()
    }
    @IBAction func onTapCancelSelectButton(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func timerCallback() {
        self.checkLocationStatus(true)
    }
    
    @objc func deniedTimerCallback() {
        if !isActive {
            return
        }
        let status = CLLocationManager.authorizationStatus();
        if ticksCount < timeToWaitLocationOn {
            ticksCount += 1
            if status != .denied {
                statusDeniedTimer?.invalidate()
                checkLocationStatus(false)
                self.locationManager(manager: self.locationManager, didChangeAuthorizationStatus: status)
            }
        } else {
            statusDeniedTimer?.invalidate()
            locationOffAlert()
        }
    }
    
    @objc func dismissKeyboard() {
        searchBarView.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        let keyboardSize = (notification.userInfo?  [UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let keyboardHeight = keyboardSize?.height
        if #available(iOS 11.0, *) {
            bottomSearchConstraint.constant = -keyboardHeight! + 36
        }
        else {
            bottomSearchConstraint.constant = 0
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        bottomSearchConstraint.constant =  0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBarView.delegate = self
        btnCancelSelection.isHidden = fromAbout
        cnstSearchViewHeight.constant = fromAbout ? 200 - 29 - 30 - 8 : 200
        cnstCancelBtnTop.constant = fromAbout ? 0 : 11
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.setValue(UIColor.white, forKeyPath: "textColor")
        pickerView.selectRow(selectedPickerIndex, inComponent: 0, animated: false)
        
        self.checkLocationStatus(false)
        
        currentLocationPanelView.navigateDelegate = self
        updateViews(showPicker: false)
        
        startListenToKeboardEvents()
        mapView.delegate = self
        self.pin = MKPointAnnotation()
        self.circle = MKCircle()
        mapView.addAnnotation(self.pin!)
        mapView.addOverlay(self.circle!)
        
        if setRadius {
            mapView.isZoomEnabled = false
            mapView.isScrollEnabled = false
            mapView.isUserInteractionEnabled = false
        }
        
        if fromSettings {
            initViewFromSettings()
        }
        
        setManifityGlassColor()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func initViewFromSettings() {
        let defaults = UserDefaults.standard
        let curLat = defaults.double(forKey: "lat")
        let curLon = defaults.double(forKey: "lon")
        let pickerIndex = defaults.value(forKey: "radiusPickerIndex")
        if pickerIndex != nil {
            selectedPickerIndex = pickerIndex as! Int
            pickerView.selectRow(selectedPickerIndex, inComponent: 0, animated: false)
        }

        let coordinate = CLLocationCoordinate2D(latitude: curLat, longitude: curLon)
        self.pin?.coordinate = coordinate
        self.mapView.addAnnotation(self.pin!)
        addCircle(coordinate)
        currentLocation = CLLocation(latitude: curLat, longitude: curLon)
        dropPinZoomIn(placemark: MKPlacemark(coordinate: coordinate))
        
        if setRadius {
            updateViews(showPicker: true)
        }
    }
    
    func saveLocation() {
        var lat = 0.0;
        var long = 0.0;
        let radius = Double(pickerValues[selectedPickerIndex]) /  3.281
        
        if let curLocation = self.currentLocation {
            lat = curLocation.coordinate.latitude
            long = curLocation.coordinate.longitude
        }
        if let searchLocation = self.searchResult?.location {
            lat = searchLocation.coordinate.latitude
            long = searchLocation.coordinate.longitude
        }
        print("\(lat) , \(long) , \(radius)")
        saveResultsToDefaults(lat: lat, long: long, radius: Int(radius)) {
            DispatchQueue.main.async {
                if !self.setRadius {
                    DataBaseService.shared.removeAll()
                }
                
                if self.fromAbout {
                    let vc = MainScreenTabBarController.makeFromStoryboard()
                    self.navigationController?.setViewControllers([vc], animated: true)
                } else {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    func saveLocationWithAlert() {
        let alert = UIAlertController(title: "Attention!", message: "Your Home Location has changed. Would you like to Reset all Statistics? All current data will be lost!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
            self.saveLocation()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func locationOffAlert() {
        let alert = UIAlertController(title: "Attention!", message: "We were not able to detect your location. Please enter it manually.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            self.updateLoadingView()
        }))
        self.present(alert, animated: true)
    }
    
    func checkLocationStatus(_ tick: Bool) {
        let status = CLLocationManager.authorizationStatus();
        if tick {
            if status != .notDetermined {
                self.locationManager(manager: self.locationManager, didChangeAuthorizationStatus: status)
                statusTimer?.invalidate()
            }
        } else {
            self.locationManager(manager: self.locationManager, didChangeAuthorizationStatus: status)
            if status == .notDetermined {
                statusTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
            }
            if status == .denied {
                statusDeniedTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(deniedTimerCallback), userInfo: nil, repeats: true)
            }
        }
    }
    
    func updateCurrentLocationName() {
        getNameByLocation(location: currentLocation!) { (reversedLocation) in
            DispatchQueue.main.async {
                self.updateLocationName(locationName: reversedLocation!.formattedAddressSmall)
                self.locationNameInitialized = true
            }
        }
    }
    
    func updateSearchResultLocationName() {
        guard (self.searchResult?.location) != nil else {
            return
        }
        self.dropPinZoomIn(placemark: self.searchResult!)
        updateViews(showPicker: true)
    }
    
    private func updateLoadingView() {
        UIView.animate(withDuration: 2, animations: {
            self.loadingContainer.alpha = 0
        }) { _ in
            self.loadingContainer.isHidden = true
        }
    }
    
    private func updateViews(showPicker: Bool) {
        pickerViewContainer.isHidden = !showPicker
        searchViewContainer.isHidden = showPicker
        searchBarView.resignFirstResponder()
        if !pickerViewContainer.isHidden {
            addCircle(nil)
        } else {
            if self.circle != nil {
                self.mapView.removeOverlay(self.circle!)
            }
        }
    }
    
    private func addCircle(_ location: CLLocationCoordinate2D?) {
        self.mapView.removeOverlay(self.circle!)
        let radius = Double(pickerValues[selectedPickerIndex]) / 3.281
        if let location = location {
            self.circle = MKCircle(center: location, radius: CLLocationDistance(radius))
            self.mapView.addOverlay(self.circle!)
            return
        }
        if let curLocation = self.currentLocation {
            self.circle = MKCircle(center: curLocation.coordinate, radius: CLLocationDistance(radius))
        }
        if let searchLocation = self.searchResult?.location {
            self.circle = MKCircle(center: searchLocation.coordinate, radius: CLLocationDistance(radius))
        }
        self.mapView.addOverlay(self.circle!)
    }
    
    private func updateLocationName(locationName: String) {
        self.currentLocationPanelView.updateCurrentLocation(locationName)
    }
    
    private func makeSearchRequest(searchText: String) {
        if searchText.count > 3 {
            let request = MKLocalSearch.Request()
            request.naturalLanguageQuery = searchText
            request.region = mapView.region
            let search = MKLocalSearch(request: request)
            search.start { (response, error) in
                if let mapItem = response?.mapItems[0] {
                    self.searchResult = mapItem.placemark
                } else {
                    self.searchResult = nil
                }
            }
        }
    }
    
    private func getNameByLocation(location: CLLocation, callback: @escaping ( _ reversedLocation: ReversedGeoLocation?)-> Void) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            guard let placemark = placemarks?.first else {
                let errorString = error?.localizedDescription ?? "Unexpected Error"
                print("Unable to reverse geocode the given location. Error: \(errorString)")
                return
            }
            
            let reversedGeoLocation = ReversedGeoLocation(with: placemark)
            callback(reversedGeoLocation)
        }
    }
    
    private func showChoiceLocation() {
        let vc = ChoiceLocationViewController.makeFromStoryboard()

        vc.mapView = mapView
        vc.onSelectPlaceMark = { placeMark in
            self.mapView.setCenter(placeMark.coordinate, animated: false)
            self.currentLocation = CLLocation.init(latitude: placeMark.coordinate.latitude,
                                              longitude: placeMark.coordinate.longitude)
        }
        
        vc.modalPresentationStyle = .formSheet
        let navigationController = UINavigationController(rootViewController: vc)
        present(navigationController, animated: true, completion: nil)
                navigationController.navigationBar.tintColor = .black
    }
    
    private func startListenToKeboardEvents() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        mapView.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func setManifityGlassColor() {
        let textField = searchBarView.value(forKey: "searchField") as! UITextField

        let glassIconView = textField.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        glassIconView.tintColor = UIColor(red: 0.922, green: 0.922, blue: 0.961, alpha: 0.6)
    }
    
    private func startListenToBgEvents() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBecomeInactive), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBecomeAactive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func onBecomeInactive(notification: Notification) {
        self.isActive = false
    }
    
    @objc func onBecomeAactive(notification: Notification) {
        self.isActive = true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
}

extension LocationScreenViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        if !fromSettings {
            currentLocation = location
            self.navigateToHomeLocation()
        }
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        print(status.rawValue)
        
        switch status {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            locationManager.requestLocation()
            break
        case .authorizedAlways:
            locationManager.requestLocation()
            break
        case .restricted:
            break
        case .denied:
            break
        default:
            break
        }
        
        prevLocationStatus = status
    }
}

extension LocationScreenViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        showChoiceLocation()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder();
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        showChoiceLocation()
        searchBar.resignFirstResponder();
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    }
}

extension LocationScreenViewController: HandleMapSearch {
    func dropPinZoomIn(placemark: MKPlacemark) {
        mapView.removeAnnotations(mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        
        mapView.addAnnotation(annotation)
        _ = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: placemark.coordinate, latitudinalMeters: 300, longitudinalMeters: 300)
        mapView.setRegion(region, animated: true)
    }
}

extension LocationScreenViewController: LocationPanelDelegate {
    func navigateToHomeLocation() {
        mapView.removeAnnotations(mapView.annotations)
        let coordinateRegion = MKCoordinateRegion(center: currentLocation!.coordinate, latitudinalMeters: 200, longitudinalMeters: 200)
        mapView.setRegion(coordinateRegion, animated: false)
    }
    
    func navigateToHomeLocationAndUpdateView() {
        if currentLocation != nil {
            self.navigateToHomeLocation()
            
            if setHomeBase {
                self.saveLocationWithAlert()
            } else {
                updateViews(showPicker: true)
            }
        }
    }
}

extension LocationScreenViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerValues.count
    }
}

extension LocationScreenViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let result = "\(pickerValues[row]) Feet"
        return result
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedPickerIndex = row
        addCircle(nil)
    }
}

extension LocationScreenViewController: UserDefaultHelper {
    func saveResultsToDefaults(lat: Double, long: Double, radius: Int, callback: @escaping ()-> Void) {
        getNameByLocation(location: currentLocation!) { (reversedLocation) in
            let defaults = UserDefaults.standard
            defaults.set(lat, forKey: "lat")
            defaults.set(long, forKey: "lon")
            defaults.set(radius, forKey: "radius")
            defaults.set(self.selectedPickerIndex, forKey: "radiusPickerIndex")
            defaults.set(reversedLocation!.formattedAddressSmall, forKey: "address")
            defaults.set(Date().timeIntervalSince1970, forKey: "startTrack")
            callback()
        }
    }
}

extension LocationScreenViewController: MKMapViewDelegate {
    
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        self.pin?.coordinate = self.mapView.centerCoordinate
        self.mapView.addAnnotation(self.pin!)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer! {
        if overlay is MKCircle {
            var circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = UIColor.blue
            circle.fillColor = UIColor(red: 0, green: 0, blue: 255, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        } else {
            return nil
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let coords = self.mapView.centerCoordinate
        if locationNameInitialized != nil && locationNameInitialized == true {
            self.currentLocation = CLLocation(latitude: coords.latitude, longitude: coords.longitude)
        }
    }
}
