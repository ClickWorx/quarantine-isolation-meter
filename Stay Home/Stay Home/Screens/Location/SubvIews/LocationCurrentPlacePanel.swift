//
//  MainScreenSmallPanel.swift
//  Stay Home
//
//  Created by Anton Trofimenko on 24.03.2020.
//

import UIKit

@IBDesignable
class LocationCurrentPlacePanel: UIView {
    let nibName = "LocationCurrentPlacePanel"
    var contentView:UIView?
    
    var navigateDelegate: LocationPanelDelegate?
    var gesture: UITapGestureRecognizer?
    
    @IBOutlet weak var touchContainer: UIView!
    @IBOutlet weak var selectBtn: UIButton!
    
    @IBAction func buttonTap(_ sender: UIButton) {
        if let delegate = self.navigateDelegate {
            delegate.navigateToHomeLocationAndUpdateView()
        }
    }
    @IBOutlet weak var locationNameLbl: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func updateCurrentLocation(_ locationName: String) {
        self.locationNameLbl.text = locationName
    }

    func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        
        gesture = UITapGestureRecognizer(target: self, action: #selector(onContainerTap))
        touchContainer.addGestureRecognizer(gesture!)
    }

    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    @objc func onContainerTap() -> Void {
        selectBtn.sendActions(for: .touchUpInside)
    }
}
