//
//  LoginViewController.swift
//  Stay Home
//
//  Created by Дмитрий Кривенко on 23.03.2020.
//

import UIKit

class LoginViewController: UIViewController {

    // MARK: Properties
    @IBOutlet var containerView: UIView!
    @IBOutlet var phoneFormView: UIView!
    @IBOutlet var codeFormView: UIView!
    @IBOutlet var enterCodeHintLabel: UILabel!
    @IBOutlet var phoneField: UITextField!
    @IBOutlet var codeStackView: UIStackView!
    @IBOutlet var timerView: UIView!
    @IBOutlet var timerValueLabel: UILabel!
    @IBOutlet var resendCodeButton: UIButton!
    @IBAction func onChangedPhoneText(_ sender: Any) {
        
    }
    @IBAction func onTapEnterButton(_ sender: Any) {
        
    }
    @IBAction func onTapContinueButton(_ sender: Any) {
        
    }
    @IBAction func onTapResendCodeButton(_ sender: Any) {
        
    }
    
    var timeToResendLess: Int = 0
    weak var resendTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
    }
    
    func initUI() {
        showPhoneForm()
        setContainerBackground()
    }
    
    func setContainerBackground() {
        UIGraphicsBeginImageContext(self.view.frame.size)
//        UIImage(named: "LoginBackground")?.draw(in: self.containerView.bounds)

        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        guard let bgImage = image else {
            return
        }
        
        containerView.backgroundColor = UIColor(patternImage: bgImage)
    }
    
    func startResendTimer(at: Int = 40) {
        
    }
    
    func stopResendTimer() {
        
    }
    
    func toggleResendTimer() {
        
    }
    
    func toggleEnableSendButton() {
        
    }
    
    func showPhoneForm() {
        phoneFormView.isHidden = false
        codeFormView.isHidden = !phoneFormView.isHidden
    }
    
    func showEnterCodeForm() {
        codeFormView.isHidden = false
        phoneFormView.isHidden = !codeFormView.isHidden

    }
}
