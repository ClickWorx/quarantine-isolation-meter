//
//  Logs.swift
//  Stay Home
//
//  Created by Anton Chikunov on 26/03/2020.
//

import Foundation
import UIKit
import MessageUI

class LogsViewController: UIViewController, Storyboardable {
    static var storyboardName: String {
        return "Logs"
    }
    
    var locations: [LocationData] = []
    
    @IBOutlet weak var table: UITableView!
    
    @IBAction func add1Home(_ sender: Any) {
        self.addLogs(count: 1, isInHome: true)
    }
    @IBAction func add89Home(_ sender: Any) {
        self.addLogs(count: 89, isInHome: true)
    }
    @IBAction func add500Home(_ sender: Any) {
        self.addLogs(count: 500, isInHome: true)
    }
    @IBAction func add1Outside(_ sender: Any) {
        self.addLogs(count: 1, isInHome: false)
    }
    @IBAction func add89Outside(_ sender: Any) {
        self.addLogs(count: 89, isInHome: false)
    }
    @IBAction func add500Outside(_ sender: Any) {
        self.addLogs(count: 500, isInHome: false)
    }
    @IBAction func sendLogs(_ sender: Any) {
        LocationService.shared.bgGeo.viewController = self
        LocationService.shared.sendLogs()
        LocationService.shared.bgGeo.viewController = nil
    }
    
    @IBAction func sendLocationsData(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            sendLocationsDataByEmail()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locations = DataBaseService.shared.getAll().sorted(by: { (a, b) -> Bool in
            return a.id > b.id
        })
        self.navigationController?.isToolbarHidden = false
        self.navigationController?.isNavigationBarHidden = false
        
        let wasAtHome = locations.filter{ $0.isInHome == true }.count
        let wasOutHome = locations.filter{ $0.isInHome == false }.count
        self.navigationItem.title = "T: \(locations.count) H: \(wasAtHome) N: \(wasOutHome)"
        
        self.table.delegate = self
        self.table.dataSource = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isToolbarHidden = true
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func addLogs(count: Int, isInHome: Bool) {
        locations = DataBaseService.shared.getAll()
        var dateToStart: Date
        if locations.count > 0 {
            dateToStart = locations[0].id
        } else {
            let startTrackSeconds = UserDefaults.standard.double(forKey: "startTrack")
            dateToStart = Date(timeIntervalSince1970: startTrackSeconds)
        }
        let calendar = Calendar.current
        print("[addLogs]", dateToStart, count)
        for _ in 1...count {
            dateToStart = calendar.date(byAdding: .minute, value: -5, to: dateToStart)!
            DataBaseService.shared.create(data: LocationData(id: dateToStart, isInHome: isInHome, lat: 0, lon: 0))
            print("[addLogs]", dateToStart)
        }
        locations = DataBaseService.shared.getAll()
        let wasAtHome = locations.filter{ $0.isInHome == true }.count
        let wasOutHome = locations.filter{ $0.isInHome == false }.count
        self.navigationItem.title = "T: \(locations.count) H: \(wasAtHome) N: \(wasOutHome)"
    }
    
    private func sendLocationsDataByEmail() {
        let mailComposeViewController = MFMailComposeViewController()
        let emailText: String = locations.map { location -> String in
            return location.description
        }.joined(separator: "\n")
        
        mailComposeViewController.delegate = self as? UINavigationControllerDelegate
        mailComposeViewController.setSubject("Locations Data")
        mailComposeViewController.setToRecipients(["anton@sharp-dev.net"])
        mailComposeViewController.setMessageBody(emailText, isHTML: false)
        
        self.present(mailComposeViewController, animated: true, completion: nil)
    }
}

extension LogsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "LogsCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? LogsCell  else {
            fatalError("The dequeued cell is not an instance of LogsCell.")
        }
        
        // Fetches the appropriate meal for the data source layout.
        let log = locations[indexPath.row]
        
        cell.lblDate?.text = log.id.description
        cell.lblHome?.text = log.isInHome ? "Home" : "No"
        cell.lblLat?.text = log.lat.description
        cell.lblLon?.text = log.lon.description
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension LogsViewController: MFMailComposeViewControllerDelegate {
    public func mailComposeController(_ controller: MFMailComposeViewController,
    didFinishWith result: MFMailComposeResult,
    error: Error?) {
        controller.dismiss(animated: true)
    }
}
