//
//  LogsCell.swift
//  Stay Home
//
//  Created by Anton Chikunov on 26/03/2020.
//

import Foundation
import UIKit

class LogsCell : UITableViewCell {
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLon: UILabel!
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblLat: UILabel!
    
}
