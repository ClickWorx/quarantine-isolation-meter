//
//  Extensions.swift
//  Stay Home
//
//  Created by Anton Trofimenko on 23.03.2020.
//

import UIKit

@IBDesignable
class ColoredImageView: UIImageView {
  @IBInspectable var changeColor: UIColor? {
      get {
          let color = UIColor(cgColor: layer.borderColor!);
          return color
      }
      set {
          let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
          self.image = templateImage
          self.tintColor = newValue
      }
  }

}

@IBDesignable
class RoundedCornerView: UIView {
  @IBInspectable var cornerRadius: CGFloat = 0 {
    didSet {
      layer.cornerRadius = cornerRadius
      layer.masksToBounds = cornerRadius > 0
    }
  }
  
  @IBInspectable var borderWidth: CGFloat = 0 {
    didSet {
      layer.borderWidth = borderWidth
    }
  }
  
  @IBInspectable var borderColor: UIColor? {
    didSet {
      layer.borderColor = borderColor?.cgColor
    }
  }
}

@IBDesignable
class RoundedSegmentedView: UIView {
  @IBInspectable var cornerRadius: CGFloat = 0 {
    didSet {
      layer.cornerRadius = cornerRadius
      layer.masksToBounds = cornerRadius > 0
    }
  }
  
  @IBInspectable var borderColor: UIColor? {
    didSet {
      layer.borderColor = borderColor?.cgColor
    }
  }
}

@IBDesignable
class SegmentedCircleView: UIView {
    @IBInspectable var safetyColor: UIColor = UIColor.init(rgb: 0x0DC758) {
        didSet { print("mainColor was set here") }
    }
    
    @IBInspectable var outsideColor: UIColor = UIColor.init(rgb: 0xFC3E30) {
        didSet { print("outsideColor was set here") }
    }
    
    @IBInspectable var nodataColor: UIColor = UIColor.init(rgb: 0x58636A) {
        didSet { print("nodataColor was set here") }
    }
    
    @IBInspectable var ringThickness: CGFloat = 4 {
        didSet { print("ringThickness was set here") }
    }
    
    @IBInspectable var safetyPercent: CGFloat = 30 {
        didSet { self.setNeedsDisplay() }
    }
    
    @IBInspectable var outsidePercent: CGFloat = 60 {
        didSet { self.setNeedsDisplay() }
    }
    
    @IBInspectable var nodataPercent: CGFloat = 10 {
        didSet { self.setNeedsDisplay() }
    }
    @IBInspectable var circleThickness: CGFloat = 30 {
        didSet { print("ringThickness was set here") }
    }

    override func draw(_ rect: CGRect) {
        let emptyColor = UIColor.init(rgb: 0x010b0d)
        let isEmpty: Bool = safetyPercent == 0 && outsidePercent == 0 && nodataPercent == 0
        if isEmpty {
            createSegment(rect, startDegree: 0, endDegree: 360, color: emptyColor)
            return
        }
        var currentDegree: CGFloat = 270.0
        var nextDegree: CGFloat = currentDegree + (360 * (safetyPercent/100))
        createSegment(rect, startDegree: currentDegree, endDegree: nextDegree, color: safetyColor)
        
        currentDegree = nextDegree
        nextDegree = currentDegree + (360 * (outsidePercent/100))
        createSegment(rect, startDegree: currentDegree, endDegree: nextDegree, color: outsideColor)
        
        currentDegree = nextDegree
        nextDegree = currentDegree + (360 * (nodataPercent/100))
        createSegment(rect, startDegree: currentDegree, endDegree: nextDegree, color: nodataColor)
    }
    
    func createSegment(_ rect: CGRect, startDegree: CGFloat, endDegree: CGFloat, color: UIColor) {
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(circleThickness)
        context?.setStrokeColor(color.cgColor)
        context?.addArc(center: CGPoint(x: rect.width/2, y: rect.height/2), radius: rect.width/2 - circleThickness/2, startAngle: deg2rad(startDegree), endAngle: deg2rad(endDegree), clockwise: false)
        context?.strokePath()
    }
    
    func deg2rad(_ number: CGFloat) -> CGFloat {
        return number * .pi / 180
    }
}

@IBDesignable
class CustomSegmentedControl: UISegmentedControl {
    @IBInspectable var textColor: UIColor = UIColor.white {
        didSet {
            let unselectedAttributes = [NSAttributedString.Key.foregroundColor: textColor,
                                        NSAttributedString.Key.font:  UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular)]
            self.setTitleTextAttributes(unselectedAttributes, for: .normal)
            self.setTitleTextAttributes(unselectedAttributes, for: .selected)
        }
    }
}

extension UIColor {
    convenience init(rgb: UInt) {
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
