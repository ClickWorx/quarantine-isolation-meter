//
//  MainScreenTabBarController.swift
//  Stay Home
//
//  Created by Anton Trofimenko on 30.03.2020.
//

import UIKit

class MainScreenTabBarController: UITabBarController, Storyboardable {

    static var storyboardName: String {
        return "MainScreen"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
