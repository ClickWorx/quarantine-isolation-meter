//
//  MainScreenViewController.swift
//  Stay Home
//
//  Created by Anton Trofimenko on 23.03.2020.
//

import UIKit

class MainScreenViewController: UIViewController, Storyboardable {
    static var storyboardName: String {
        return "MainScreen"
    }
    
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    
    private var spacingForContainer: CGFloat {
        var resultSpacing: CGFloat = 35.0
        
        switch screenHeight {
        case 896:
            resultSpacing = 40.0
            break
        case 812:
            resultSpacing = 30.0
            break
        case 736:
            resultSpacing = 30.0
            break
        case 667:
            resultSpacing = 15.0
            break
        default:
            resultSpacing = 35.0
            break
        }
        
        return resultSpacing
    }
    
    private var heightForSmallPanel: CGFloat {
        var resultHeight: CGFloat = 82.0
        
        switch screenHeight {
        case 896:
            resultHeight = 100.0
            break
        case 667:
            resultHeight = 72.0
            break
        default:
            resultHeight = 82.0
            break
        }
        
        return resultHeight
    }
    
    private var heightForLargePanel: CGFloat {
        var resultHeight: CGFloat = 60.0
        
        switch screenHeight {
        case 896:
            resultHeight = 75.0
            break
        case 667:
            resultHeight = 50.0
            break
        default:
            resultHeight = 60.0
            break
        }
        
        return resultHeight
    }
    
    private var isolationScore: String = ""
    private var todayDate: String = ""
    private var datePeriod: [Int] = [1, 2, 7]
    private var selectedPeriodIndex: Int = 0
    private var selectedPeriodStat: Stat = Stat(inHome: 0, outHome: 0, noData: 0)
    private var safetyCurTime = "0h 0m"
    private var outsideCurTime = "0h 0m"
    private var noDataCurTime = "0h 0m"
    private var currentLocationName = ""
    private var isInHouse: Bool?
    private var isActive = true
    
    @IBOutlet weak var lblIsolationScore: UILabel!
    @IBOutlet weak var switchPeriod: CustomSegmentedControl!
    @IBOutlet weak var lblToday: UILabel!
    @IBOutlet weak var barGraph: SegmentedCircleView!
    @IBOutlet weak var viewSafetyPlace: MainScreenSmallPanel!
    @IBOutlet weak var viewOutside: MainScreenSmallPanel!
    @IBOutlet weak var viewNoData: MainScreenLargePanel!
    @IBOutlet weak var statusPanelView: MainScreenStatPanel!
    @IBOutlet weak var mainContainer: UIStackView!
    
    @IBOutlet weak var smallPanelHeight: NSLayoutConstraint!
    @IBOutlet weak var largePanelHeight: NSLayoutConstraint!
    
    @IBAction func shareToSocial(_ sender: UIButton) {
        let daysFromDb = DataBaseService.shared.getAll()
        var days = 0
        if daysFromDb.count > 0 {
            let wasAtHome = daysFromDb.filter{ $0.isInHome == true }.count
            days = wasAtHome / 288
        }
        share(msg: "Hey, I am using the 'Quarantine: Isolation Meter' app on my iPhone. I stayed home during \(days) days and my Isolation score is \(self.isolationScore)")
    }
    
    @objc func todayLabelTapped() {
        let vc = LogsViewController.makeFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func isolationLabelTapped() {
        LocationService.shared.bgGeo.viewController = self
        LocationService.shared.sendLogs()
        LocationService.shared.bgGeo.viewController = nil
    }
    
    @objc func onPeriodSelected(sender: UISegmentedControl) -> Void {
        selectedPeriodIndex = sender.selectedSegmentIndex
        updateData()
        updateViews()
    }
    
    @objc func onBecomeInactive(notification: Notification) {
        self.isActive = false
    }
    
    @objc func onBecomeAactive(notification: Notification) {
        self.isActive = true
        self.updateData()
        self.updateViews()
        LocationService.shared.checkFolLocationAlert()
    }
    
    override func viewDidLoad() {
        self.checkScore()
        switchPeriod.addTarget(self, action: #selector(onPeriodSelected(sender:)), for: .valueChanged)
        
        getInitialPosition {
            DispatchQueue.main.async {
                self.updateViews()
            }
        }
        
        mainContainer.spacing = self.spacingForContainer
        smallPanelHeight.constant = self.heightForSmallPanel
        largePanelHeight.constant = self.heightForLargePanel
        self.updateData()
        self.updateViews()
        self.locationTrackingStart()
        self.startListenToBgEvents()
        
        let todayLabelTap = UITapGestureRecognizer(target: self, action: #selector(todayLabelTapped))
        self.lblToday.isUserInteractionEnabled = true
        self.lblToday.addGestureRecognizer(todayLabelTap)
        
        LocationService.shared.addHomeGeofence()
        LocationService.shared.checkFolLocationAlert()
        askForNotificationsPermission()
    }
    
    private func askForNotificationsPermission() {
        NotificationsService.shared.userRequest()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func checkScore() -> Void {
        let iScore = getScore()
        UserDefaults.standard.set(iScore, forKey: "score")
    }
    
    func locationTrackingStart() {
        LocationService.shared.start()
        LocationService.shared.startWatch(onComplete: { (inHouse) in
            self.isInHouse = inHouse
            if self.isActive {
                DispatchQueue.main.async {
                    self.updateData()
                    self.updateViews()
                }
            }
        }, onError: { () in
            self.isInHouse = nil
            if self.isActive {
                DispatchQueue.main.async {
                    self.updateData()
                    self.updateViews()
                }
            }})
    }
    
    func updateData() {
        let curDatePeriod = datePeriod[selectedPeriodIndex]
        self.selectedPeriodStat = getStat(days: curDatePeriod)
        
        self.todayDate = getCurrentDate()
        
        checkScore()
        let curScore = UserDefaults.standard.integer(forKey: "score")
        self.isolationScore = "\(curScore)%"
        
        self.currentLocationName = UserDefaults.standard.string(forKey: "address") ?? ""
        
        self.safetyCurTime = calcTime(curDatePeriod: curDatePeriod, periodStat: selectedPeriodStat.inHome)
        self.outsideCurTime = calcTime(curDatePeriod: curDatePeriod, periodStat: selectedPeriodStat.outHome)
        self.noDataCurTime = calcTime(curDatePeriod: curDatePeriod, periodStat: selectedPeriodStat.noData)
    }
    
    func updateViews() {
        self.lblToday.text = self.todayDate
        self.lblIsolationScore.text = self.isolationScore
        self.barGraph.safetyPercent = CGFloat(self.selectedPeriodStat.inHome)
        self.barGraph.outsidePercent = CGFloat(self.selectedPeriodStat.outHome)
        self.barGraph.nodataPercent = CGFloat(self.selectedPeriodStat.noData)
        viewSafetyPlace.updateTypeAndTime(panelType: .Safety, time: self.safetyCurTime)
        viewOutside.updateTypeAndTime(panelType: .Outside, time: self.outsideCurTime)
        viewNoData.updateTypeAndTime(panelType: .NoData, time: self.noDataCurTime)
        var panelType = PanelType.NoData
        if isInHouse != nil {
            panelType = isInHouse! ? PanelType.Safety : PanelType.Outside
        }
        statusPanelView.updateType(panelType: panelType)
    }
    
    func getInitialPosition(onCompleteCallback: @escaping () -> Void) {
        LocationService.shared.getCurrentLocation(onComplete: { (result) in
            self.isInHouse = result
            onCompleteCallback()
        }, onError: { () in
            self.isInHouse = nil
            onCompleteCallback()
        })
    }
    
    func share(msg: String) {
        let activityVC = UIActivityViewController(activityItems: [msg], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        self.present(activityVC, animated: true, completion: nil)
    }
    
    private func calcTime(curDatePeriod: Int, periodStat: Double) -> String {
        let secondsTotal = Float(curDatePeriod * 24 * 3600)
        let safetySeconds = secondsTotal * Float(Float(periodStat) / 100)
        let safetySecondsFormatted = timeString(time: TimeInterval(Int(safetySeconds)))
        return safetySecondsFormatted
    }
    
    private func startListenToBgEvents() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBecomeInactive), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onBecomeAactive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
}
