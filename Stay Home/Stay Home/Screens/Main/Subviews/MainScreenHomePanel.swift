//
//  MainScreenLargePanel.swift
//  Stay Home
//
//  Created by Anton Trofimenko on 23.03.2020.
//

import UIKit

@IBDesignable
class MainScreenHomePanel: UIView {
    let nibName = "MainScreenHomePanel"
    var contentView:UIView?

    @IBOutlet weak var label: UILabel!

    @IBAction func buttonTap(_ sender: UIButton) {
        label.text = "Hi"
    }
    @IBOutlet weak var btnChange: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
    }

    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func updateLocation(locationName: String) {
        label.text = locationName
    }
}
