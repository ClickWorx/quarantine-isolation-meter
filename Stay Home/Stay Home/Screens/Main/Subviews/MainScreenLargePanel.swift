//
//  MainScreenLargePanel.swift
//  Stay Home
//
//  Created by Anton Trofimenko on 23.03.2020.
//

import UIKit

@IBDesignable
class MainScreenLargePanel: PanelView {
    override var nibName: String {
        get {
            return "MainScreenLargePanel"
        }
    }
}
