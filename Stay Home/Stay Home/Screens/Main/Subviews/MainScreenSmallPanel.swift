//
//  MainScreenSmallPanel.swift
//  Stay Home
//
//  Created by Anton Trofimenko on 23.03.2020.
//

import UIKit

@IBDesignable
class MainScreenSmallPanel: PanelView {
    override var nibName: String {
        get {
            return "MainScreenSmallPanel"
        }
    }
}
