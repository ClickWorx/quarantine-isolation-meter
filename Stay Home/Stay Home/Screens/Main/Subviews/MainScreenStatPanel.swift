//
//  MainScreenSmallPanel.swift
//  Stay Home
//
//  Created by Anton Trofimenko on 25.03.2020.
//

import UIKit

@IBDesignable
class MainScreenStatPanel: UIView {
    var nibName: String {
        get {
            return "MainScreenStatPanel"
        }
    }
    var contentView: UIView?

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageBg: RoundedCornerView!
    @IBOutlet weak var imageStatus: UIImageView!
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
    }

    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func updateType(panelType: PanelType) {
        var imageName = ""
        var title = ""
        var bgColor = UIColor(red: 13/255, green: 199/255, blue: 89/255, alpha: 0.20)
        
        switch panelType {
            case .Safety:
                imageName = "at_home"
                title = "Now At Home"
                bgColor = UIColor(red: 13/255, green: 199/255, blue: 89/255, alpha: 0.20)
                break
            case .Outside:
                imageName = "at_outside"
                title = "Now Outside"
                bgColor = UIColor(red: 252/255, green: 62/255, blue: 48/255, alpha: 0.20)
                break
            case .NoData:
                imageName = "no_data"
                title = "No Data"
                bgColor = UIColor(red: 88/255, green: 99/255, blue: 106/255, alpha: 0.20)
                break
        }
        
        let image = UIImage(named: imageName)
        lblTitle.text = title
        imageStatus.image = image
        imageBg.backgroundColor = bgColor
    }
}
