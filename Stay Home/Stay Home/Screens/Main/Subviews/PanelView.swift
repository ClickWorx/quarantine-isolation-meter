//
//  MainScreenSmallPanel.swift
//  Stay Home
//
//  Created by Anton Trofimenko on 25.03.2020.
//

import UIKit

enum PanelType {
    case Safety
    case Outside
    case NoData
}

@IBDesignable
class PanelView: UIView {
    var nibName: String {
        get {
            return "MainScreenSmallPanel"
        }
    }
    var contentView: UIView?

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imagePlace: UIImageView!
    @IBOutlet weak var bgContainer: RoundedCornerView!
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        bgContainer.backgroundColor = UIColor(red: 0.129, green: 0.137, blue: 0.149, alpha: 1)
    }

    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func updateTypeAndTime(panelType: PanelType, time: String) {
        var imageName = ""
        var title = ""
        
        switch panelType {
            case .Safety:
                imageName = "home"
                title = "Home Base"
                break
            case .Outside:
                imageName = "outside"
                title = "Outside"
                break
            case .NoData:
                imageName = "nodata"
                title = "No Data"
                break
        }
        
        let image = UIImage(named: imageName)
        lblTitle.text = title
        imagePlace.image = image
        lblTime.text = time
    }
}
