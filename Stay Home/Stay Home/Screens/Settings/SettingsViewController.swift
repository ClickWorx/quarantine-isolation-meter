//
//  SettingsViewController.swift
//  Stay Home
//
//  Created by Дмитрий Кривенко on 30.03.2020.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet var settingsTableView: UITableView!
    
    var radiusPickerIndex: Int = 0 {
        didSet {
            settingsTableView.reloadData()
        }
    }
    
    var address: String = "" {
        didSet {
            settingsTableView.reloadData()
        }
    }
    
    private let pickerValues = [Int](50...500).filter { (i) -> Bool in
        return i % 10 == 0
    }
    
    
    override func viewDidLoad() {
                    let bundle = Bundle(for: type(of: self))
        settingsTableView.register(UINib(nibName: "HomeBaseSettingsRow", bundle: bundle), forCellReuseIdentifier: "HomeBaseSettingsRow")
        settingsTableView.register(UINib(nibName: "RadiusSettingsRow", bundle: bundle), forCellReuseIdentifier: "RadiusSettingsRow")
        settingsTableView.register(UINib(nibName: "MenuSettingsRow", bundle: bundle), forCellReuseIdentifier: "MenuSettingsRow")
        settingsTableView.register(UINib(nibName: "ResetStatisticsRow", bundle: bundle), forCellReuseIdentifier: "ResetStatisticsRow")
        settingsTableView.delegate = self
        settingsTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let defaults = UserDefaults.standard
        radiusPickerIndex = defaults.integer(forKey: "radiusPickerIndex")
        address = defaults.string(forKey: "address") ?? ""
    }
    
    private func loadViewFromNib(nibName: String) -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch MenuSections(rawValue: indexPath.section) {
        case .homeBaseSection:
            let locationVC = LocationScreenViewController.makeFromStoryboard()
            locationVC.fromSettings = true
            locationVC.setHomeBase = true
            self.navigationController?.pushViewController(locationVC, animated: true)
        case .radiusSection:
            let locationVC = LocationScreenViewController.makeFromStoryboard()
            locationVC.fromSettings = true
            locationVC.setRadius = true
            self.navigationController?.pushViewController(locationVC, animated: true)
        case .resetSection:
            let refreshAlert = UIAlertController(title: "Reset Statistics", message: "Are you sure you want to reset statistics?", preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                self.resetStatistics()
            }))

            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel))

            present(refreshAlert, animated: true, completion: nil)
        default:
            switch MenuPoints(rawValue: indexPath.row) {
            case .about:
                let aboutVC = AboutScreenViewController.makeFromStoryboard()
                
                aboutVC.fromSettings = true
                navigationController?.pushViewController(aboutVC, animated: true)
            case .IsolationScoreFaq:
                if let IsolationScoreFaqURL = URL(string: "http://quaratine-tracking-meter.com") {
                    UIApplication.shared.open(IsolationScoreFaqURL)
                }
            case .sourceCode:
                if let sourceCodeURL = URL(string: "https://bitbucket.org/ClickWorx/quarantine-isolation-meter.git") {
                    UIApplication.shared.open(sourceCodeURL)
                }
            case .privacy:
                if let privacyURL = URL(string: "http://quaratine-tracking-meter.com") {
                    UIApplication.shared.open(privacyURL)
                }
            case .none:
                print("Unknown menu point")
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func resetStatistics() {
        DispatchQueue.main.async {
            DataBaseService.shared.removeAll()
            UserDefaults.standard.set(Date().timeIntervalSince1970, forKey: "startTrack")
        }
    }
}

extension SettingsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch MenuSections(rawValue: section) {
        case .settingsMenuSection:
            return 4
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch MenuSections(rawValue: indexPath.section) {
        case .homeBaseSection:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeBaseSettingsRow") as! HomeBaseSettingsRow
            
            cell.backgroundColor = UIColor(red: 0.129, green: 0.137, blue: 0.149, alpha: 1)
            cell.selectionStyle = .none
            cell.addressLabel.text = address
            
            return cell
        case .radiusSection:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RadiusSettingsRow") as! RadiusSettingsRow
            
            cell.backgroundColor = UIColor(red: 0.129, green: 0.137, blue: 0.149, alpha: 1)
            cell.selectionStyle = .none
            cell.radiusValueLabel.text = "\(pickerValues[radiusPickerIndex]) feet"

            return cell
        case .resetSection:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ResetStatisticsRow") as! ResetStatisticsRow
            cell.backgroundColor = UIColor(red: 0.129, green: 0.137, blue: 0.149, alpha: 1)
            cell.selectionStyle = .none
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuSettingsRow") as! MenuSettingsRow
            switch MenuPoints(rawValue: indexPath.row) {
            case .about:
                cell.menuItemLabel.text = "About"
            case .IsolationScoreFaq:
                cell.menuItemLabel.text = "Isolation Score FAQ"
            case .sourceCode:
                cell.menuItemLabel.text = "Source Code"
            case .privacy:
                cell.menuItemLabel.text = "Privacy"
            case .none:
                cell.menuItemLabel.text = ""
            }
            cell.backgroundColor = UIColor(red: 0.129, green: 0.137, blue: 0.149, alpha: 1)
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = UIColor(rgb: 0x303030)
    }

    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = UIColor(red: 0.129, green: 0.137, blue: 0.149, alpha: 1)
    }
    
    func numberOfSections(in: UITableView) -> Int {
        return 4
    }
}

enum MenuPoints: Int {
    case about = 0, IsolationScoreFaq, sourceCode, privacy
}

enum MenuSections: Int {
    case homeBaseSection = 0, radiusSection, settingsMenuSection, resetSection
}
