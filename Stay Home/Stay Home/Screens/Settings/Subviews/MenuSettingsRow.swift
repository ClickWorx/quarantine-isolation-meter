//
//  File.swift
//  Stay Home
//
//  Created by Дмитрий Кривенко on 30.03.2020.
//

import UIKit

@IBDesignable
class MenuSettingsRow: UITableViewCell {
    @IBOutlet var menuItemLabel: UILabel!
}

