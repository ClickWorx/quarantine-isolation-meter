//
//  DataBaseService.swift
//  Stay Home
//
//  Created by Дмитрий Кривенко on 24.03.2020.
//

import Foundation
import SQLite

class DataBaseService {
    var db: Connection?
    let locationsTable: Table = Table("locations")
    
    static let shared = DataBaseService()

    init() {
        let path = NSSearchPathForDirectoriesInDomains(
            .documentDirectory, .userDomainMask, true
        ).first!

        do {
            db = try Connection("\(path)/quarantine.db")
            
            
        } catch {
            print("No database connection!")
        }
        
        let columns = getColumns()
        let createQuery = locationsTable.create(temporary: false, ifNotExists: true, withoutRowid: true) { (t) in
            t.column(columns.0, primaryKey: true)
            t.column(columns.1)
            t.column(columns.2)
            t.column(columns.3)
            
        }
        
        do {
            _ = try db?.run(createQuery)
        } catch {
            print("Can't create table Locations!")
        }
        
        
    }
    
    func create(data: LocationData) {
        let columns = getColumns()
        
        let insert = locationsTable.insert(columns.0 <- data.id,
                                           columns.1 <- data.isInHome,
                                           columns.2 <- data.lat,
                                           columns.3 <- data.lon)
        
        do {
            _ = try db?.run(insert)
        } catch  {
            print("Error creating row!", error)
        }
    }
    
    func read(rowId: Date) -> LocationData? {
        let columns = getColumns()
        let query = locationsTable.select(columns.0, columns.1, columns.2, columns.3)
            .filter(columns.0 == rowId)
        
        var locations: [LocationData] = []
        
        do {
            for location in try db!.prepare(query) {
                locations.append(LocationData(id: location[columns.0],
                                    isInHome: location[columns.1],
                                    lat: location[columns.2],
                                    lon: location[columns.3]))
            }
        } catch {
            print("Not find row")
        }

        return locations.first
    }
    
    func update(rowId: Date, data: LocationData) {
        // @TODO Implement
    }
    
    func delete(rowId: Date) {
        let columns = getColumns()
        let query = locationsTable.filter(columns.0 == rowId).delete()
        
        do {
            _ = try db?.run(query)
        } catch {
            print("Error deleting rows!")
        }
    }
    
    func getAll() -> [LocationData] {
        let columns = getColumns()
        let query = locationsTable.select(columns.0, columns.1, columns.2, columns.3)
        var locations: [LocationData] = []
        
        do {
            for location in try db!.prepare(query) {
                locations.append(LocationData(id: location[columns.0],
                                    isInHome: location[columns.1],
                                    lat: location[columns.2],
                                    lon: location[columns.3]))
            }
        } catch {
            print("No rows!")
        }
        locations.sort { (a, b) -> Bool in
            return a.id < b.id
        }
        return locations
    }
    
    func getAll(from: Date, to: Date) -> [LocationData] {
        let columns = getColumns()
        let query = locationsTable.select(columns.0, columns.1, columns.2, columns.3)
            .filter(columns.0 >= from && columns.0 <= to)
        var locations: [LocationData] = []
        
        do {
            for location in try db!.prepare(query) {
                locations.append(LocationData(id: location[columns.0],
                                    isInHome: location[columns.1],
                                    lat: location[columns.2],
                                    lon: location[columns.3]))
            }
        } catch {
            print("No rows at date!")
        }

        return locations
    }
    
    func removeAll() {
        do {
            _ = try db?.run(locationsTable.delete())
        } catch {
            print("Error deleting rows!")
        }
    }
    
    private func getColumns() -> (Expression<Date>, Expression<Bool>, Expression<Double>, Expression<Double>) {
        return((Expression<Date>("id"),
                Expression<Bool>("isInHome"),
                Expression<Double>("lat"),
                Expression<Double>("lon")
        ))
    }
}
