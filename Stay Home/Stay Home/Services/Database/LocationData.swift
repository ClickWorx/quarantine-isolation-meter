//
//  LocationData.swift
//  Stay Home
//
//  Created by Дмитрий Кривенко on 24.03.2020.
//

import Foundation

struct LocationData {
    var description: String {
        get {
            return "\(id.description))|\(isInHome ? "Home" : "No")|\(lat.description)|\(lon.description)"
        }
    }
    
    let id: Date
    let isInHome: Bool
    let lat: Double
    let lon: Double
}
