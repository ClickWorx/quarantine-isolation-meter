//
//  LocationService.swift
//  Stay Home
//
//  Created by Anton Trofimenko on 24.03.2020.
//

import Foundation
import CoreLocation
import TSLocationManager

protocol LocationServiceProtocol {
    func start()
    func stop()
    func getCurrentLocation(onComplete: @escaping (Bool) -> Void, onError: @escaping () -> Void)
    func startWatch(onComplete: @escaping (Bool) -> Void, onError: @escaping () -> Void)
    func stopWatch()
    func sendLogs()
}

protocol PluginListenerProtocol {
    func onLocation(location: TSLocation?)
    func onHeartbeat(event: TSHeartbeatEvent?)
    func onGeofence(event: TSGeofenceEvent?)
    func onProvider(event: TSProviderChangeEvent?)
}

class LocationService {
    
    static let shared = LocationService()
    
    var bgGeo: TSLocationManager
    var config: TSConfig
    
    var queue: DispatchQueue
    
    init() {
        queue = DispatchQueue.init(label: "LocationQueue")
        bgGeo = TSLocationManager.sharedInstance()
        config = TSConfig.sharedInstance()
        bgGeo.onHeartbeat { (event) in
            self.onHeartbeat(event: event)
        }
        bgGeo.onLocation({ (location) in
            self.onLocation(location: location)
        }) { (error) in
            print("[Location:onLocation] failer")
            self.locationFailer(error: error)
        }
        bgGeo.onGeofence { (event) in
            self.onGeofence(event: event)
        }
        bgGeo.onProviderChange { (providerEvent) in
            self.onProvider(event: providerEvent)
        }
        config.update({ (builder) in
            builder?.desiredAccuracy = kCLLocationAccuracyBest
            builder?.distanceFilter = 10;
            builder?.stationaryRadius = 5
            builder?.stopTimeout = 60
            builder?.stopOnTerminate = false;
            builder?.startOnBoot = true;
            builder?.heartbeatInterval = 60 * 5
            builder?.preventSuspend = true
            builder?.geofenceInitialTriggerEntry = false
            builder?.geofenceProximityRadius = 1000
            builder?.debug = false
            builder?.logLevel = tsLogLevelDebug
        })
        bgGeo.ready()
    }
    
    func addHomeGeofence() {
        bgGeo.removeGeofences()
        let lat = UserDefaults.standard.double(forKey: "lat")
        let lon = UserDefaults.standard.double(forKey: "lon")
        let radius = UserDefaults.standard.integer(forKey: "radius")
        bgGeo.add(TSGeofence(identifier: "HOME", radius: CLLocationDistance(radius), latitude: lat, longitude: lon, notifyOnEntry: true, notifyOnExit: true, notifyOnDwell: false, loiteringDelay: 10000), success: {
            print("[addHomeGeofence] added: ", lat, lon, radius)
        }) { (errorString) in
            print("[addHomeGeofence] error: ", errorString, lat, lon, radius)
        }
    }
    
    private func locationFailer(error: Error?) -> Void {
        print("\(error?.localizedDescription)")
    }
    
    private func locationSuccess(location: TSLocation?) -> Void {
        guard let curLat = location?.location.coordinate.latitude,
            let curLon = location?.location.coordinate.longitude
            else { return }
        
        let isInHome = self.isAtHome(curLat: curLat, curLon: curLon)
        
        let dbService = DataBaseService.shared
        
        let now = Date()
        var newRecord = LocationData(id: now,
                                     isInHome: isInHome,
                                     lat: curLat,
                                     lon: curLon)
        
        var oldRecords = self.checkOldData(now: now,isInHouse: isInHome)
        if oldRecords.count > 0 {
            let lastOldRecord = oldRecords.last!
            if lastOldRecord.id.distance(to: now) / 60 < 5 {
                oldRecords.popLast()
                newRecord = LocationData(id: lastOldRecord.id, isInHome: isInHome,
                lat: curLat,
                lon: curLon)
                oldRecords.append(newRecord)
            }
        } else {
            oldRecords.append(newRecord)
        }
        for record in oldRecords {
            dbService.create(data: record)
        }
        let iScore = getScore()
        
        let curScore = UserDefaults.standard.integer(forKey: "score")
        UserDefaults.standard.set(iScore, forKey: "score")
        
        if (curScore != iScore) {
            let curScoreMod = curScore % 10
            let newScoreMod = iScore % 10
            var inc = false
            var displayScore = -1
            if abs(iScore - curScore) >= 10 {
                if iScore > curScore {
                    inc = iScore > curScore
                    for i in (curScore...iScore).reversed() {
                        if i % 10 == 0 {
                            displayScore = i
                            break
                        }
                    }
                } else {
                    inc = false
                    for i in iScore...curScore {
                        if i % 10 == 0 {
                            displayScore = i
                            break
                        }
                    }
                }
            } else {
                if newScoreMod == 0 && curScoreMod != 0 {
                    inc = iScore > curScore
                    displayScore = iScore
                }
            }
            if (displayScore == -1) {
                return
            }
            let msg = inc ? "Congratulations, you just reached \(displayScore)% Isolation score" : "Your Isolation score just dropped to \(displayScore)%. Stay Home!"
            NotificationsService.shared.scheduleNotification(msg: msg)
        }
    }
    
    func checkOldData(now: Date, isInHouse: Bool) -> [LocationData] {
        let dbService = DataBaseService.shared
        let locations = dbService.getAll()
        let lastLocation = locations.last
        if lastLocation != nil || locations.count == 0 {
            let startTrackSeconds = UserDefaults.standard.double(forKey: "startTrack")
            let startTrackDate = Date(timeIntervalSince1970: startTrackSeconds)
            let fromDateToFill = lastLocation?.id ?? startTrackDate
            let atHomeToFill = lastLocation?.isInHome ?? isInHouse
            let latToFill = lastLocation?.lat ?? UserDefaults.standard.double(forKey: "lat")
            let lonToFill = lastLocation?.lon ?? UserDefaults.standard.double(forKey: "lon")
            let diff = fromDateToFill.distance(to: now) / 60
            print("[locationSuccess]", diff, fromDateToFill, now, lastLocation, startTrackDate)
            if diff > 5 {
                var oldRecords: [LocationData] = []
                let calendar = Calendar.current
                var tickDate = calendar.date(byAdding: .minute, value: 5, to: fromDateToFill)!
                while tickDate < now {
                    print("[locationSuccess]", tickDate)
                    oldRecords.append(LocationData(id: tickDate,
                                                   isInHome: atHomeToFill,
                                                   lat: latToFill,
                                                   lon: lonToFill))
                    tickDate = calendar.date(byAdding: .minute, value: 5, to: tickDate)!
                }
                return oldRecords
            }
        }
        return []
    }
    
    private func canStartTrack() -> Bool {
        var canStart = false
        
        let db = DataBaseService.shared
        let locations = db.getAll()
        let now = Date()
        if locations.count > 0 {
            let lastLocation = db.getAll().last
            canStart = lastLocation!.id.distance(to: now) / 60 >= 5
            print("[canStartTrack]", lastLocation!.id, now)
        } else {
            let startTrackSeconds = UserDefaults.standard.double(forKey: "startTrack")
            let startTrackDate = Date(timeIntervalSince1970: startTrackSeconds)
            canStart = startTrackDate.distance(to: now) / 60 >= 5
            print("[canStartTrack]", startTrackDate, now)
        }
        
        return canStart
    }
    
    private func isAtHome(curLat: Double, curLon: Double) -> Bool {
        let lat = UserDefaults.standard.double(forKey: "lat")
        let lon = UserDefaults.standard.double(forKey: "lon")
        let radius = UserDefaults.standard.integer(forKey: "radius")
        let distance = getDistance(lat1:lat, lon1:lon, lat2:curLat, lon2:curLon, unit:"K") * Double(1000)
        return distance < Double(radius)
    }
}

extension LocationService: LocationServiceProtocol {
    func start() {
        if (!config.enabled) {
            bgGeo.start()
        }
    }
    
    func stop() {
        bgGeo.stop()
    }
    
    func getCurrentLocation(onComplete: @escaping (Bool) -> Void, onError: @escaping () -> Void) {
        let request = TSCurrentPositionRequest(persist: true, samples: 3, success: { (location) in
            guard let curLat = location?.location.coordinate.latitude,
                let curLon = location?.location.coordinate.longitude else {
                    onError()
                    return
            }
            let isInHome = self.isAtHome(curLat: curLat, curLon: curLon)
            onComplete(isInHome)
        }, failure: { (_) in
            onError()
        })
        bgGeo.getCurrentPosition(request)
    }
    
    
    func startWatch(onComplete: @escaping (Bool) -> Void, onError: @escaping () -> Void) {
        let request = TSWatchPositionRequest(interval: 10000, persist: true, desiredAccuracy: kCLLocationAccuracyBest, extras: nil, timeout: 60000, success: { (location) in
            guard let curLat = location?.location.coordinate.latitude,
                let curLon = location?.location.coordinate.longitude
                else { return }
            
            let isInHome = self.isAtHome(curLat: curLat, curLon: curLon)
            onComplete(isInHome)
            self.queue.sync {
                if self.canStartTrack() {
                    self.locationSuccess(location: location)
                }
            }
        }, failure: { (error) in
            onError()
        })
        
        bgGeo.watchPosition(request)
    }
    
    func stopWatch() {
        bgGeo.stopWatchPosition()
    }
    
    func sendLogs() {
        bgGeo.emailLog("anton@sharp-dev.net", success: {
            print("[Location:sendLogs:success] logs sent")
        }) { (error) in
            print("[Location:sendLogs:error] \(error?.debugDescription)")
        }
    }
}

extension LocationService: PluginListenerProtocol {
    func onLocation(location: TSLocation?) {
        print("[Location:onLocation] success")
        self.queue.sync {
            if self.canStartTrack() {
                self.locationSuccess(location: location)
            }
        }
    }
    
    func onGeofence(event: TSGeofenceEvent?) {
        let lat = UserDefaults.standard.double(forKey: "lat")
        let lon = UserDefaults.standard.double(forKey: "lon")
        let radius = UserDefaults.standard.integer(forKey: "radius")
        let curLat = event?.location.location.coordinate.latitude
        let curLon = event?.location.location.coordinate.longitude
        let distance = getDistance(lat1:lat, lon1:lon, lat2:curLat!, lon2:curLon!, unit:"K") * Double(1000)
        print("[Location:onGeofence]", event?.action, distance)
    }
    
    func onHeartbeat(event: TSHeartbeatEvent?) {
        print("[Location:onHeartbeat] triggered")
        let request = TSCurrentPositionRequest(persist: true, samples: 3, success: { location in
            self.queue.sync {
                if self.canStartTrack() {
                    self.locationSuccess(location: location)
                }
            }
        }, failure: self.locationFailer)
        bgGeo.getCurrentPosition(request)
    }
    
    func onProvider(event: TSProviderChangeEvent?) -> Void {
        print(event ?? "empty TSProviderChangeEvent")
    }
    
    func checkFolLocationAlert() {
        let status = CLLocationManager.authorizationStatus()
        if status != .authorizedAlways {
            let alert = UIAlertController(title: "Location permission required", message: "\nApplication can't work without location permission set to 'Always'\n\n1. In Settings, select Location\n2. Then tap on Always", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default, handler: { _ in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)")
                    })
                }
            }))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true)
        }
    }
}
