//
//  distance.swift
//  Stay Home
//
//  Created by Anton Chikunov on 25/03/2020.
//

import Foundation
////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///  This routine calculates the distance between two points (given the      ///
///  latitude/longitude of those points). It is being used to calculate      ///
///  the distance between two location using GeoDataSource(TM)               ///
///  products.                                                               ///
///                                                                          ///
///  Definitions:                                                            ///
///    South latitudes are negative, east longitudes are positive            ///
///                                                                          ///
///  Passed to function:                                                     ///
///    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)   ///
///    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)   ///
///    unit = the unit you desire for results                                ///
///           where: 'M' is statute miles (default)                          ///
///                  'K' is kilometers                                       ///
///                  'N' is nautical miles                                   ///
///                                                                          ///
///  Worldwide cities and other features databases with latitude longitude   ///
///  are available at https://www.geodatasource.com                           ///
///                                                                          ///
///  For enquiries, please contact sales@geodatasource.com                   ///
///                                                                          ///
///  Official Web site: https://www.geodatasource.com                         ///
///                                                                          ///
///  GeoDataSource.com (C) All Rights Reserved 2016                          ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
///  This function converts decimal degrees to radians              ///
///////////////////////////////////////////////////////////////////////
func deg2rad(deg:Double) -> Double {
    return deg * M_PI / 180
}

///////////////////////////////////////////////////////////////////////
///  This function converts radians to decimal degrees              ///
///////////////////////////////////////////////////////////////////////
func rad2deg(rad:Double) -> Double {
    return rad * 180.0 / M_PI
}

func getDistance(lat1:Double, lon1:Double, lat2:Double, lon2:Double, unit:String) -> Double {
    let theta = lon1 - lon2
    var dist = sin(deg2rad(deg: lat1)) * sin(deg2rad(deg: lat2)) + cos(deg2rad(deg: lat1)) * cos(deg2rad(deg: lat2)) * cos(deg2rad(deg: theta))
    dist = acos(dist)
    dist = rad2deg(rad: dist)
    dist = dist * 60 * 1.1515
    if (unit == "K") {
        dist = dist * 1.609344
    }
    else if (unit == "N") {
        dist = dist * 0.8684
    }
    return dist
}

struct Stat {
    var inHome: Double
    var outHome: Double
    var noData: Double
}


func getStat(days: Int) -> Stat {
    let dbService = DataBaseService.shared
    let locationDataList: [LocationData] = dbService.getAll().sorted { (a, b) -> Bool in
        return a.id > b.id
    }
    var partLocations: [LocationData] = []
    if locationDataList.count < 288 * days {
        partLocations = locationDataList
    } else {
        partLocations = Array(locationDataList[0...288 * days - 1])
    }

    let wasAtHome = partLocations.filter{ $0.isInHome == true }.count
    let wasOutHome = partLocations.filter{ $0.isInHome == false }.count
    print("[getStat]", partLocations.count, wasAtHome, wasOutHome, days)
    
    let inHome = Double(wasAtHome) / Double(288 * days) * 100
    let outHome = Double(wasOutHome) / Double(288 * days) * 100
    let noData = 100 - inHome - outHome
    print("[getStat]", inHome, outHome, noData)
    
    return Stat(inHome: inHome, outHome: outHome, noData: noData)
}

func getScore() -> Int {
    let dayStat = getStat(days: 1)
    let weekStat = getStat(days: 7)
    let dblWeekStat = getStat(days: 14)
    
    let iScore = Int(0.25 * dayStat.inHome + 0.25 * weekStat.inHome + 0.50 * dblWeekStat.inHome)
    print("[getScore]", iScore, dayStat.inHome, weekStat.inHome, dblWeekStat.inHome)
    return iScore
}

func getNextScore(score: Int) -> Int {
    return score - (score % 10) + 10
}

func getCurrentDate() -> String {
    let date = Date()
    let calendar = Calendar.current
    let components = calendar.dateComponents([.year, .month, .day], from: date)

    let year =  components.year
    let month = DateFormatter().monthSymbols[(components.month ?? 1) - 1]
    let day = components.day
    
    return "Today, \(day ?? 0) \(month) \(year ?? 0)"
}

func timeString(time: TimeInterval) -> String {
    var hour = Int(time) / 3600
    var minute = Int(time) / 60 % 60
    var minuteDelta = minute % 10
    
    minute = minute - minuteDelta
    if minuteDelta <= 3 {
        minuteDelta = 0
    } else if minuteDelta <= 7 {
        minuteDelta = 5
    } else {
        minuteDelta = 10
    }
    minute = minute + minuteDelta
    if minute == 60 {
        hour += 1
        minute = 0
    }
    return String(format: "%02ih %02im", hour, minute)
}
