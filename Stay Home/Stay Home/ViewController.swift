//
//  ViewController.swift
//  Stay Home
//
//  Created by Anton Chikunov on 23/03/2020.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let controller  = PositionViewController.makeFromStoryboard()
        self.navigationController?.pushViewController(controller, animated: false)
    }


}

