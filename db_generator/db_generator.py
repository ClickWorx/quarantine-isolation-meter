import sqlite3
from datetime import date, datetime, timedelta
from random import random

def generate_db(day_count, day=288):
    conn = sqlite3.connect(f'quarantine.db{day_count}')
    c = conn.cursor()
    c.execute('''CREATE TABLE "locations" 
                (
                    "id" TEXT PRIMARY KEY NOT NULL, 
                    "isInHome" INTEGER NOT NULL,
                    "lat" REAL NOT NULL,
                    "lon" REAL NOT NULL
                ) WITHOUT ROWID''')

    now = datetime.now() - timedelta(days=1)
    delta = timedelta(minutes=5)
    for i in range(day * day_count):
        c.execute(f"INSERT INTO locations VALUES ('{now.isoformat()}',{int(random() > 0.3)},55.755786,37.617633)")
        now -= delta

    conn.commit()
    conn.close()

if __name__ == "__main__":
    generate_db(1, 10)